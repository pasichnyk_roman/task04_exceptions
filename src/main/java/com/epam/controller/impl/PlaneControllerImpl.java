package com.epam.controller.impl;

import com.epam.controller.PlaneController;
import com.epam.exceptions.AlreayExistsException;
import com.epam.model.CivilPlaneBusinessLogic;
import com.epam.model.HeavyPlaneBusinessLogic;
import com.epam.model.domains.BasicPlaneDomain;
import com.epam.model.enums.PlaneType;

import java.io.*;
import java.util.*;


/**
 * This class implements all availability methods
 * in the application.
 */
public class PlaneControllerImpl implements PlaneController {

    /**
     * Scanner.
     */
    private static Scanner sc = new Scanner(System.in);
    /**
     * Buffered Reader use for plane name and guns.
     */
    private static BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));

    /**
     * The Civil plane object.
     */
    private CivilPlaneBusinessLogic civilPlaneBusinessLogic =
            new CivilPlaneBusinessLogic();
    /**
     * The Heavy plane object.
     */
    private HeavyPlaneBusinessLogic heavyPlaneBusinessLogic =
            new HeavyPlaneBusinessLogic();

    /**
     * ArrayList of all planes.
     */
    private List<BasicPlaneDomain> planes = new ArrayList<>();

    /**
     * The method add plane to the ArrayList - planes.
     *
     * @throws Exception - the method works with scanner and buffered reader.
     */
    @Override
    public final void addPlane() throws Exception {
        String planeName = null;
        while (true) {
            boolean nameExists = false;
            try {
                System.out.println("Enter the plane name");
                planeName = br.readLine();
                for (BasicPlaneDomain plane : planes) {
                    if (planeName.equals(plane.getName())) {
                        throw new AlreayExistsException("The plane with name '" + planeName + "' is already exists");
                    }
                }
                break;
            } catch (AlreayExistsException e) {
                e.printStackTrace();
            }
        }
        int maxWeight = 0;
        while(true) {
            System.out.println("Enter the max weight");
            try {
                maxWeight = Integer.parseInt(br.readLine());
                break;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
//        while (!sc.hasNextInt()) {
//            System.out.println("Enter the max weight");
//            sc.next();
//        }
//        int maxWeight = sc.nextInt();

        System.out.println("Enter the max amount of people");
        while (!sc.hasNextInt()) {
            System.out.println("Enter the max amount of people");
            sc.next();
        }
        int maxPeople = sc.nextInt();

        System.out.println("Enter the flight range");
        while (!sc.hasNextInt()) {
            System.out.println("Enter the flight range");
            sc.next();
        }
        int flightRange = sc.nextInt();

        System.out.println("Enter the Fuel Consumption");
        while (!sc.hasNextFloat()) {
            System.out.println("Enter the Fuel Consumption");
            sc.next();
        }
        float fuelConsumption = sc.nextFloat();

        System.out.println("Select plane type: 1 - Heavy and 2 - Civil");
        while (!sc.hasNextByte()) {
            System.out.println("Select plane type: 1 - Heavy and 2 - Civil");
            sc.next();
        }
        byte type = sc.nextByte();
        boolean cargoCompartment;
        if (type == 1) {
            System.out.println("Enter list of guns");
            String guns = br.readLine();
            planes.add(heavyPlaneBusinessLogic.addPlane(
                    planeName, maxWeight, maxPeople, PlaneType.HEAVY,
                    fuelConsumption, flightRange, guns));
        } else {
            String answer;
            do {
                System.out.println(
                        "Specify available cargo compartment - Yes or No");
                answer = sc.next().toUpperCase();
            } while (!answer.equals("YES") && !answer.equals("NO"));
            cargoCompartment = answer.equals("YES");
            planes.add(civilPlaneBusinessLogic.addPlane(
                    planeName, maxWeight, maxPeople, PlaneType.CIVIL,
                    fuelConsumption, flightRange, cargoCompartment));
        }

    }

    /**
     * The method prints all planes in the console.
     * All object from the planes ArrayList.
     */
    @Override
    public final void printAllPlanes() {
        for (BasicPlaneDomain plane : planes) {
            System.out.println(plane);
        }
    }

    /**
     * The method calculate sum of weight in all planes.
     *
     * @return sum all weight.
     */
    @Override
    public final int maxAllWeight() {
        int maxAllWeight = 0;
        for (BasicPlaneDomain plane : planes) {
            maxAllWeight += plane.getMaxWeight();
        }
        return maxAllWeight;
    }

    /**
     * The method calculate the sum of all people.
     *
     * @return sum of all people.
     */
    @Override
    public final int maxAllPeople() {
        int maxAllPeople = 0;
        for (BasicPlaneDomain plane : planes) {
            maxAllPeople += plane.getMaxPeople();
        }
        return maxAllPeople;
    }

    /**
     * the method find plane in some range.
     * User enter range.
     */
    @Override
    public final void findPlaneByFuel() {
        List<BasicPlaneDomain> planesByFuel = new ArrayList<>();

        System.out.println("Enter the range [a] of Fuel Consumption");
        while (!sc.hasNextFloat()) {
            System.out.println("Enter the range [a] of Fuel Consumption");
            sc.next();
        }
        float fuelConsumptionStart = sc.nextFloat();
        System.out.println("Enter the range [b] of Fuel Consumption");
        while (!sc.hasNextFloat()) {
            System.out.println("Enter the range [b] of Fuel Consumption");
            sc.next();
        }
        float fuelConsumptionEnd = sc.nextFloat();

        if (fuelConsumptionStart > fuelConsumptionEnd) {
            float buf;
            buf = fuelConsumptionEnd;
            fuelConsumptionEnd = fuelConsumptionStart;
            fuelConsumptionStart = buf;
        }

        for (BasicPlaneDomain plane : planes) {
            if (plane.getFuelConsumption() >= fuelConsumptionStart
                    && plane.getFuelConsumption() <= fuelConsumptionEnd) {
                planesByFuel.add(plane);
            }
        }
        System.out.println(
                "Only planes which have fuel consumption in the range between "
                        + fuelConsumptionStart + " and "
                        + fuelConsumptionEnd + ":");
        for (BasicPlaneDomain plane : planesByFuel) {
            System.out.println(plane);
        }

    }

    /**
     * the method sort all planes by flight range.
     */
    @Override
    public final void sortPlaneByFlightRange() {
        planes.sort(Comparator.comparingInt(BasicPlaneDomain::getFlightRange));
        for (BasicPlaneDomain plane : planes) {
            System.out.println(plane);
        }
    }

    @Override
    public void addPlanesToFile() throws IOException {
        FileWriter fw = new FileWriter("Planes.txt");
        for (BasicPlaneDomain plane : planes) {
            try {
                fw.write(plane.toString() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fw.close();
        System.out.println("List of planes successfully added ");
    }
}
