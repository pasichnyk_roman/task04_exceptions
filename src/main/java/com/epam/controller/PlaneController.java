package com.epam.controller;

import com.epam.model.domains.BasicPlaneDomain;

import java.io.IOException;
import java.util.List;

/**
 * This interface display all availability methods
 * in the application with planes.
 */

public interface PlaneController {

    /**
     * add plane in the ArrayList.
     * @throws Exception - method works with scanner and buffered reader.
     */
    void addPlane() throws Exception;

    /**
     * the method prints all planes in the console.
     */
    void printAllPlanes();

    /**
     * the method display the sum of weight.
     * @return the sum of weight.
     */
    int maxAllWeight();

    /**
     * the method display the sum of all people.
     * @return the sum of all people.
     */
    int maxAllPeople();

    /**
     * the method find plane in some range.
     * User enter range.
     */
    void findPlaneByFuel();

    /**
     * the method sort all planes by flight range.
     */
    void sortPlaneByFlightRange();

    void addPlanesToFile() throws IOException;

}
