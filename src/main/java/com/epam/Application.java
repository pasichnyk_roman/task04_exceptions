package com.epam;

import com.epam.view.MyView;

/**
 * <h1>Airline</h1>
 * This application add planes to ArrayList.
 * Calculate max weight and max amount of people in all planes.
 * Sort planes by flight range.
 * Find planes by fuel Consumption in some range (user enter range).
 *
 * @author Roman Pasichnyk
 * @version 1.0
 * @since 12.11.2019
 */

public final class Application {

    /**
     * Main constructor.
     */
    private Application() {
    }

    /**
     * The application run from this method.
     * @param args - for command-line arguments.
     * @throws Exception - The I/O use in the application.
     */
    public static void main(final String[] args) {

        MyView myView = new MyView();
        myView.show();

    }

}
