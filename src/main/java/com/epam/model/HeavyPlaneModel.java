package com.epam.model;

import com.epam.model.domains.HeavyPlaneDomain;
import com.epam.model.enums.PlaneType;

/**
 * Interface display all methods for business logic civil plane.
 */
public interface HeavyPlaneModel {

    /**
     * Add plane.
     * @param name - plane name.
     * @param maxWeight - plane weight
     * @param maxPeople - max people.
     * @param planeType - plane type.
     * @param fuelConsumption - fuel consumption.
     * @param flightRange - flight range
     * @param guns - guns.
     * @return Heavy plane.
     */
    HeavyPlaneDomain addPlane(
            String name, int maxWeight, int maxPeople, PlaneType planeType,
            float fuelConsumption, int flightRange, String guns);

    /**
     * Print heavy plane in the console.
     * @return heavy plane.
     */
    HeavyPlaneDomain printPlane();

}
