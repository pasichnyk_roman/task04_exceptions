package com.epam.model.domains;

import com.epam.model.enums.PlaneType;

/**
 * The class extend Basic class of planes.
 * This class display one type of the Plane - Heavy.
 */
public class HeavyPlaneDomain extends BasicPlaneDomain {

    /**
     * guns of heave plane.
     */
    private String guns;

    /**
     * Empty constructor.
     */
    public HeavyPlaneDomain() {
    }

    /**
     * The constructor initializes all fields.
     * @param name - plane name.
     * @param maxWeight - the weight of a plane.
     * @param maxPeople - the maximum seating capacity.
     * @param planeType - plane type.
     * @param fuelConsumption - the fuel consumption.
     * @param flightRange - flight range.
     * @param guns - guns.
     */
    public HeavyPlaneDomain(final String name, final int maxWeight,
                            final int maxPeople, final PlaneType planeType,
                            final float fuelConsumption, final int flightRange,
                            final String guns) {
        super(name, maxWeight, maxPeople,
                planeType, fuelConsumption, flightRange);
        this.guns = guns;
    }

    /**
     * Getter for the guns field.
     * @return all guns.
     */
    public final String getGuns() {
        return guns;
    }

    /**
     * Setter for guns field.
     * @param guns - guns.
     */
    public final void setGuns(final String guns) {
        this.guns = guns;
    }

    /**
     * To String.
     * @return all fields of this class and parent class.
     */
    @Override
    public final String toString() {
        return "HeavyPlaneDomain{"
                + super.toString()
                + ", guns=" + guns + '}';
    }
}
