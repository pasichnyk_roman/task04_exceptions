package com.epam.model.domains;

import com.epam.model.enums.PlaneType;

/**
 * The class extend Basic class of planes.
 * This class display one type of the Plane - Civil.
 */

public class CivilPlaneDomain extends BasicPlaneDomain {

    /**
     * Availability cargo compartment in the plane.
     */
    private boolean cargoCompartment;

    /**
     * Empty constructor.
     */
    public CivilPlaneDomain() {
    }

    /**
     * Constructor initializes all fields.
     * @param name - plane name.
     * @param maxWeight - max weight of plane.
     * @param maxPeople - the maximum seating capacity.
     * @param planeType - plane type.
     * @param fuelConsumption - fuel consumption.
     * @param flightRange - flight range.
     * @param cargoCompartment - cargo compartment.
     */
    public CivilPlaneDomain(final String name, final int maxWeight,
                            final int maxPeople, final PlaneType planeType,
                            final float fuelConsumption, final int flightRange,
                            final boolean cargoCompartment) {
        super(name, maxWeight, maxPeople,
                planeType, fuelConsumption, flightRange);
        this.cargoCompartment = cargoCompartment;
    }

    /**
     * Getter for cargoCompartment field.
     * @return cargo compartment.
     */
    public final boolean isCargoCompartment() {
        return cargoCompartment;
    }

    /**
     * Setter for the cargoCompartment field.
     * @param cargoCompartment - cargo compartment.
     */
    public final void setCargoCompartment(final boolean cargoCompartment) {
        this.cargoCompartment = cargoCompartment;
    }

    /**
     * To String.
     * @return all fields of this class and parent class.
     */
    @Override
    public final String toString() {
        return "CivilPlaneDomain{"
                + super.toString()
                + ", cargoCompartment=" + cargoCompartment + '}';
    }
}
