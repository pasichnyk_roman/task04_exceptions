package com.epam.model.domains;

import com.epam.model.enums.PlaneType;

/**
 * The class display all general methods and fields.
 * Other classes can extend this class to create new type of Plane.
 */

public class BasicPlaneDomain {

    /**
     * Plane name.
     */
    private String name;

    /**
     * The maximum weight that the plane can take on board.
     */
    private int maxWeight;

    /**
     * The maximum seating capacity.
     */
    private int maxPeople;

    /**
     * Plane type. Field use PlaneType enum.
     */
    private PlaneType planeType;

    /**
     * The fuel consumption of the plane.
     */
    private float fuelConsumption;

    /**
     * The maximum amount of flight range.
     */
    private int flightRange;

    /**
     * Empty constructor.
     */
    public BasicPlaneDomain() {
    }

    /**
     * Constructor initializes all fields.
     * @param name - plane name.
     * @param maxWeight - max weight of plane.
     * @param maxPeople - the maximum seating capacity.
     * @param planeType - plane type.
     * @param fuelConsumption - fuel consumption.
     * @param flightRange - flight range.
     */
    public BasicPlaneDomain(
            final String name, final int maxWeight,
            final int maxPeople, final PlaneType planeType,
            final float fuelConsumption, final int flightRange) {
        this.name = name;
        this.maxWeight = maxWeight;
        this.maxPeople = maxPeople;
        this.planeType = planeType;
        this.fuelConsumption = fuelConsumption;
        this.flightRange = flightRange;
    }

    /**
     * Getter for the name field.
     * @return name of the plane.
     */
    public final String getName() {
        return name;
    }

    /**
     * Setter for the name field.
     * @param name - plane name.
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * Getter for the maxWeight field.
     * @return max weight.
     */
    public final int getMaxWeight() {
        return maxWeight;
    }

    /**
     * Setter for the maxWeight field.
     * @param maxWeight - max weight.
     */
    public final void setMaxWeight(final int maxWeight) {
        this.maxWeight = maxWeight;
    }

    /**
     * Getter for the maxPeople field.
     * @return max amount of people.
     */
    public final int getMaxPeople() {
        return maxPeople;
    }

    /**
     * Setter for the maxPeople field.
     * @param maxPeople - max people.
     */
    public final void setMaxPeople(final int maxPeople) {
        this.maxPeople = maxPeople;
    }

    /**
     * Getter for the planeType field.
     * @return plane type.
     */
    public final PlaneType getPlaneType() {
        return planeType;
    }

    /**
     * Setter for planeType field.
     * @param planeType - plane type.
     */
    public final void setPlaneType(final PlaneType planeType) {
        this.planeType = planeType;
    }

    /**
     * Getter for fuelConsumption field.
     * @return fuel consumption.
     */
    public final float getFuelConsumption() {
        return fuelConsumption;
    }

    /**
     * Setter for fuelConsumption field.
     * @param fuelConsumption - fuel consumption.
     */
    public final void setFuelConsumption(final float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    /**
     * Getter for the flightRange field.
     * @return flight range.
     */
    public final int getFlightRange() {
        return flightRange;
    }

    /**
     * Setter for the flightRange field.
     * @param flightRange - flight range.
     */
    public final void setFlightRange(final int flightRange) {
        this.flightRange = flightRange;
    }

    /**
     * To String for class.
     * @return all fields.
     */
    @Override
    public String toString() {
        return "BasicPlaneDomain{"
                + "name='" + name + '\''
                + ", maxWeight=" + maxWeight
                + ", maxPeople=" + maxPeople
                + ", planeType=" + planeType
                + ", fuelConsumption=" + fuelConsumption
                + ", flightRange=" + flightRange + '}';
    }
}
