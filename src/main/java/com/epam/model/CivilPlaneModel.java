package com.epam.model;

import com.epam.model.domains.CivilPlaneDomain;
import com.epam.model.enums.PlaneType;

/**
 * Interface display all methods for business logic civil plane.
 */
public interface CivilPlaneModel {

    /**
     * Add plane.
     * @param name - plane name.
     * @param maxWeight - plane weight
     * @param maxPeople - max people.
     * @param planeType - plane type.
     * @param fuelConsumption - fuel consumption.
     * @param flightRange - flight range
     * @param cargoCompartment - cargo compartment.
     * @return Civil Plane.
     */
    CivilPlaneDomain addPlane(
            String name, int maxWeight, int maxPeople, PlaneType planeType,
            float fuelConsumption, int flightRange, boolean cargoCompartment);

    /**
     * Print Civil plane in the console.
     * @return civil plane.
     */
    CivilPlaneDomain printPlane();

}
