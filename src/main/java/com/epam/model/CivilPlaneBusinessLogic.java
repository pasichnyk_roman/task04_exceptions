package com.epam.model;

import com.epam.model.domains.CivilPlaneDomain;
import com.epam.model.enums.PlaneType;

/**
 * This class implement business logic for Civil Plane.
 */
public class CivilPlaneBusinessLogic implements CivilPlaneModel {

    /**
     * Civil plane.
     */
    private CivilPlaneDomain civilPlaneDomain;

    /**
     * Empty constructor.
     */
    public CivilPlaneBusinessLogic() {

    }

    /**
     * Method add civil plane.
     * @param name - plane name.
     * @param maxWeight - plane weight
     * @param maxPeople - max people.
     * @param planeType - plane type.
     * @param fuelConsumption - fuel consumption.
     * @param flightRange - flight range
     * @param cargoCompartment - cargo compartment.
     * @return civil plane.
     */
    @Override
    public final CivilPlaneDomain addPlane(
            final String name, final int maxWeight, final int maxPeople,
            final PlaneType planeType, final float fuelConsumption,
            final int flightRange, final boolean cargoCompartment) {
        civilPlaneDomain = new CivilPlaneDomain(name, maxWeight, maxPeople,
                planeType, fuelConsumption, flightRange, cargoCompartment);
        return civilPlaneDomain;
    }


    /**
     * Print Civil plane in the console.
     * @return civil plane.
     */
    @Override
    public final CivilPlaneDomain printPlane() {
        return civilPlaneDomain;
    }

}
