package com.epam.model.enums;

/**
 * This enum display all types of planes that Application contains.
 */

public enum PlaneType {

    /**
     * Plane types.
     */
    HEAVY, CIVIL

}
