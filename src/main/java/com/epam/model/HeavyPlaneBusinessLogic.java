package com.epam.model;

import com.epam.model.domains.HeavyPlaneDomain;
import com.epam.model.enums.PlaneType;

/**
 * This class implement business logic for Heavy Plane.
 */
public class HeavyPlaneBusinessLogic implements HeavyPlaneModel {

    /**
     * Heavy plane.
     */
    private HeavyPlaneDomain heavyPlaneDomain;

    /**
     * Add heavy plane.
     * @param name - plane name.
     * @param maxWeight - plane weight
     * @param maxPeople - max people.
     * @param planeType - plane type.
     * @param fuelConsumption - fuel consumption.
     * @param flightRange - flight range
     * @param guns - guns.
     * @return heavy plane.
     */
    @Override
    public final HeavyPlaneDomain addPlane(
            final String name, final int maxWeight, final int maxPeople,
            final PlaneType planeType, final float fuelConsumption,
            final int flightRange, final String guns) {
        heavyPlaneDomain = new HeavyPlaneDomain(
                name, maxWeight, maxPeople, planeType,
                fuelConsumption, flightRange, guns);
        return heavyPlaneDomain;
    }

    /**
     * Print heavy plane in the console.
     * @return heavy plane.
     */
    @Override
    public final HeavyPlaneDomain printPlane() {
        return heavyPlaneDomain;
    }
}
