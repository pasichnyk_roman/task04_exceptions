package com.epam.view;

import com.epam.controller.impl.PlaneControllerImpl;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class print menu of application.
 */
public class MyView {

    /**
     * Plane Controller object.
     */
    private PlaneControllerImpl planeController;
    /**
     * Map that contains all menu.
     */
    private Map<String, String> menu;
    /**
     * Map that contains all methods menu.
     */
    private Map<String, Printable> methodsMenu;
    /**
     * Scanner to input some text.
     */
    private static Scanner input = new Scanner(System.in);

    /**
     * Constructor initializes planeController and
     * creates LinkedHashMap for application menu.
     */
    public MyView() {
        planeController = new PlaneControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add Plane");
        menu.put("2", "  2 - print all planes");
        menu.put("3", "  3 - print max weight in all planes together");
        menu.put("4", "  4 - print max amount of people in all planes");
        menu.put("5", "  5 - find planes by fuel consumption");
        menu.put("6", "  6 - sort planes by flight range");
        menu.put("7", "  7 - save all planes in file");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    /**
     * The method adds the plane to the ArrayList.
     * @throws Exception - Method use IO (Scanner and Buffered Reader).
     */
    private void pressButton1() throws Exception {
        planeController.addPlane();
    }

    /**
     * The method prints all planes.
     */
    private void pressButton2() {
        planeController.printAllPlanes();
    }

    /**
     * The method prints max weight of all planes in the list.
     */
    private void pressButton3() {
        System.out.println("Max Weight: " + planeController.maxAllWeight());
    }

    /**
     * The method prints max amount of people of all planes in the list.
     */
    private void pressButton4() {
        System.out.println("Max amount of people: "
                + planeController.maxAllPeople());
    }

    /**
     * User enter the Fuel Consumption range.
     * The method prints all planes in this range.
     */
    private void pressButton5() {
        planeController.findPlaneByFuel();
    }

    /**
     * The method prints sorted planes by flight range.
     */
    private void pressButton6() {
        planeController.sortPlaneByFlightRange();
    }

    private void pressButton7() throws IOException {
        planeController.addPlanesToFile();
    }

    /**
     * The method prints all menu.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * The method prints all points menu.
     */
    public final void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
